# ASSIGNMENT 8
# Michael Tehranian

""" Assignment 8 - Panoramas

This file has a number of functions that you need to fill out in order to
complete the assignment. Please write the appropriate code, following the
instructions on which functions you may or may not use.

GENERAL RULES:
    1. DO NOT INCLUDE code that saves, shows, displays, writes the image that
    you are being passed in. Do that on your own if you need to save the images
    but the functions should NOT save the image to file.

    2. DO NOT import any other libraries aside from those that we provide.
    You may not import anything else, and you should be able to complete
    the assignment with the given libraries (and in many cases without them).

    3. DO NOT change the format of this file. You may NOT change function
    type signatures (not even named parameters with defaults). You may add
    additional code to this file at your discretion, however it is your
    responsibility to ensure that the autograder accepts your submission.

    4. This file has only been tested in the provided virtual environment.
    You are responsible for ensuring that your code executes properly in the
    virtual machine environment, and that any changes you make outside the
    areas annotated for student code do not impact your performance on the
    autograder system.
"""
import numpy as np
import scipy as sp
import cv2


def getImageCorners(image):
    """Return the x, y coordinates for the four corners of an input image

    Parameters
    ----------
    image : numpy.ndarray
        Input can be a grayscale or color image

    Returns
    -------
    numpy.ndarray(dtype=np.float32)
        Array of shape (4, 1, 2).  The precision of the output is required
        for compatibility with the cv2.warpPerspective function.

    Notes
    -----
        (1) Review the documentation for cv2.perspectiveTransform (which will
        be used on the output of this function) to see the reason for the
        unintuitive shape of the output array.

        (2) When storing your corners, they must be in (X, Y) order -- keep
        this in mind and make SURE you get it right.
    """
    corners = np.zeros((4, 1, 2), dtype=np.float32)

    rows, cols = image.shape[:2]
    # Top left corner
    corners[0] = [0,0]
    # Bottom left corner
    corners[1] = [0,rows]
    # Top right corner
    corners[2] = [cols,0]
    # Bottom right corner
    corners[3] = [cols,rows]

    return corners


def findMatchesBetweenImages(image_1, image_2, num_matches):
    """Return the top list of matches between two input images.

    Parameters
    ----------
    image_1 : numpy.ndarray
        The first image (can be a grayscale or color image)

    image_2 : numpy.ndarray
        The second image (can be a grayscale or color image)

    num_matches : int
        The number of keypoint matches to find. If there are not enough,
        return as many matches as you can.

    Returns
    -------
    image_1_kp : list<cv2.KeyPoint>
        A list of keypoint descriptors from image_1

    image_2_kp : list<cv2.KeyPoint>
        A list of keypoint descriptors from image_2

    matches : list<cv2.DMatch>
        A list of the top num_matches matches between the keypoint descriptor
        lists from image_1 and image_2

    Notes
    -----
        (1) You will not be graded for this function. This function is almost
        identical to the function in Assignment 7 (we just parametrized the
        number of matches). We expect you to use the function you wrote in
        A7 here.

        (2) Python functions can return multiple values by listing them
        separated by spaces. Ex.

            def foo():
                return [], [], []
    """
    matches = None       # type: list of cv2.DMath
    image_1_kp = None    # type: list of cv2.KeyPoint items
    image_1_desc = None  # type: numpy.ndarray of numpy.uint8 values.
    image_2_kp = None    # type: list of cv2.KeyPoint items.
    image_2_desc = None  # type: numpy.ndarray of numpy.uint8 values.

    orb = cv2.ORB()
    ## This is for the Investigation part of the assignment only.
    # orb = cv2.ORB(nfeatures=500, scaleFactor=1.2, WTA_K=2, scoreType=cv2.ORB_HARRIS_SCORE,
    #               patchSize=31)

    # Find the keypoints and descriptors with ORB
    image_1_kp, keypoint_descriptors_1 = orb.detectAndCompute(image_1, None)
    image_2_kp, keypoint_descriptors_2 = orb.detectAndCompute(image_2, None)

    # Create BFMatcher object
    bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors
    matches = bf_matcher.match(keypoint_descriptors_1, keypoint_descriptors_2)

    # Sort them in the order of their distance
    matches = sorted(matches, key = lambda d_match: d_match.distance)

    # Select the first num_matches
    matches = matches[:num_matches]

    # We coded the return statement for you. You are free to modify it -- just
    # make sure the tests pass.
    return image_1_kp, image_2_kp, matches


def findHomography(image_1_kp, image_2_kp, matches):
    """Returns the homography describing the transformation between the
    keypoints of image 1 and image 2.

        ************************************************************
          Before you start this function, read the documentation
                  for cv2.DMatch, and cv2.findHomography
        ************************************************************

    Follow these steps:

        1. Iterate through matches and store the coordinates for each
           matching keypoint in the corresponding array (e.g., the
           location of keypoints from image_1_kp should be stored in
           image_1_points).

            NOTE: Image 1 is your "query" image, and image 2 is your
                  "train" image. Therefore, you index into image_1_kp
                  using `match.queryIdx`, and index into image_2_kp
                  using `match.trainIdx`.

        2. Call cv2.findHomography() and pass in image_1_points and
           image_2_points, using method=cv2.RANSAC and
           ransacReprojThreshold=5.0.

        3. cv2.findHomography() returns two values: the homography and
           a mask. Ignore the mask and return the homography.

    Parameters
    ----------
    image_1_kp : list<cv2.KeyPoint>
        A list of keypoint descriptors in the first image

    image_2_kp : list<cv2.KeyPoint>
        A list of keypoint descriptors in the second image

    matches : list<cv2.DMatch>
        A list of matches between the keypoint descriptor lists

    Returns
    -------
    numpy.ndarray(dtype=np.float64)
        A 3x3 array defining a homography transform between image_1 and image_2
    """
    image_1_points = np.zeros((len(matches), 1, 2), dtype=np.float32)
    image_2_points = np.zeros((len(matches), 1, 2), dtype=np.float32)

    for index, dmatch in enumerate(matches):
        image_1_points[index] = image_1_kp[dmatch.queryIdx].pt
        image_2_points[index] = image_2_kp[dmatch.trainIdx].pt

    # RANSAC-based robust method
    homography, _ = cv2.findHomography(image_1_points, image_2_points, cv2.RANSAC,
                                       ransacReprojThreshold=5.0)

    return homography


def getBoundingCorners(image_1, image_2, homography):
    """Find the coordinates of the top left corner and bottom right corner of a
    rectangle bounding a canvas large enough to fit both the warped image_1 and
    image_2.

    Given the 8 corner points (the transformed corners of image 1 and the
    corners of image 2), we want to find the bounding rectangle that
    completely contains both images.

    Follow these steps:

        1. Use getImageCorners() on image 1 and image 2 to get the corner
           coordinates of each image.

        2. Use the homography to transform the perspective of the corners from
           image 1 (but NOT image 2) to get the location of the warped
           image corners.

        3. Get the boundaries in each dimension of the enclosing rectangle by
           finding the minimum x, maximum x, minimum y, and maximum y.

        4. Store the minimum values in min_xy, and the maximum values in max_xy

    Parameters
    ----------
    image_1 : numpy.ndarray
        A grayscale or color image

    image_2 : numpy.ndarray
        A grayscale or color image

    homography : numpy.ndarray(dtype=np.float64)
        A 3x3 array defining a homography transform between image_1 and image_2

    Returns
    -------
    numpy.ndarray
        2-element array containing (x_min, y_min) -- the coordinates of the
        top left corner of the bounding rectangle of a canvas large enough to
        fit both images

    numpy.ndarray
        2-element array containing (x_max, y_max) -- the coordinates of the
        bottom right corner of the bounding rectangle of a canvas large enough
        to fit both images

    Notes
    -----
        (1) The inputs may be either color or grayscale, but they will never
        be mixed; both images will either be color, or both will be grayscale.

        (2) Python functions can return multiple values by listing them
        separated by spaces. Ex.

            def foo():
                return [], [], []
    """

    # Corner coordinates for each image
    image_1_corners = getImageCorners(image_1)
    image_2_corners = getImageCorners(image_2)

    # Transform the perspective of the corners from image_1 to get the
    # location of the warped image corners
    transformed_image_1_corners = cv2.perspectiveTransform(image_1_corners,
                                                           homography)

    # Enclose a rectangle around both images to create a bounded image
    # Dimensions are (8, 1, 2)
    bounded_image = np.concatenate((transformed_image_1_corners, image_2_corners))

    # Find the coordinates of the top left and bottom right corners
    min_xy = np.amin(bounded_image[:,0,:], axis=0)
    max_xy = np.amax(bounded_image[:,0,:], axis=0)

    return min_xy, max_xy


def warpCanvas(image, homography, min_xy, max_xy):
    """Warps the input image according to the homography transform and embeds
    the result into a canvas large enough to fit the next adjacent image
    prior to blending/stitching.

    Follow these steps:

        1. Create a translation matrix (numpy.ndarray) that will shift
           the image by x_min and y_min. This looks like this:

            [[1, 0, -x_min],
             [0, 1, -y_min],
             [0, 0, 1]]

        2. Compute the dot product of your translation matrix and the
           homography in order to obtain the homography matrix with a
           translation.

        NOTE: Matrix multiplication (dot product) is not the same thing
              as the * operator (which performs element-wise multiplication).
              See Numpy documentation for details.

        3. Call cv2.warpPerspective() and pass in image 1, the combined
           translation/homography transform matrix, and a vector describing
           the dimensions of a canvas that will fit both images.

        NOTE: cv2.warpPerspective() is touchy about the type of the output
              shape argument, which should be an integer.

    Parameters
    ----------
    image : numpy.ndarray
        A grayscale or color image

    homography : numpy.ndarray(dtype=np.float64)
        A 3x3 array defining a homography transform between two sequential
        images in a panorama sequence

    min_xy : numpy.ndarray
        2x1 array containing the coordinates of the top left corner of a
        canvas large enough to fit the warped input image and the next
        image in a panorama sequence

    max_xy : numpy.ndarray
        2x1 array containing the coordinates of the bottom right corner of
        a canvas large enough to fit the warped input image and the next
        image in a panorama sequence

    Returns
    -------
    numpy.ndarray
        An array containing the warped input image embedded in a canvas
        large enough to join with the next image in the panorama

    Notes
    -----
        (1) You must explain the reason for multiplying x_min and y_min
        by negative 1 in your writeup.
    """
    # canvas_size properly encodes the size parameter for cv2.warpPerspective,
    # which requires a tuple of ints to specify size, or else it may throw
    # a warning/error, or fail silently
    canvas_size = tuple(np.round(max_xy - min_xy).astype(np.int))

    min_x = min_xy[0]
    min_y = min_xy[1]

    # Create a translation matrix that will shift the image by x_min and y_min
    translation = np.array([[1, 0, -1 * min_x], [0, 1, -1 * min_y], [0, 0, 1]])

    # Compute the dot product of your translation matrix and the
    # homography in order to obtain the homography matrix with a
    # translation
    homographic_trans = np.dot(translation, homography)

    image_warped = cv2.warpPerspective(image, homographic_trans, canvas_size)

    return image_warped


def blendImagePair(image_1, image_2, num_matches):
    """This function takes two images as input and fits them onto a single
    canvas by performing a homography warp on image_1 so that the keypoints
    in image_1 aligns with the matched keypoints in image_2.

    **************************************************************************

        You MUST replace the basic insertion blend provided here to earn
                         credit for this function.

       The most common implementation is to use alpha blending to take the
       average between the images for the pixels that overlap, but you are
                    encouraged to use other approaches.

           Be creative -- good blending is the primary way to earn
                  Above & Beyond credit on this assignment.

    **************************************************************************

    Parameters
    ----------
    image_1 : numpy.ndarray
        A grayscale or color image

    image_2 : numpy.ndarray
        A grayscale or color image

    num_matches : int
        The number of keypoint matches to find between the input images

    Returns:
    ----------
    numpy.ndarray
        An array containing both input images on a single canvas

    Notes
    -----
        (1) This function is not graded by the autograder. It will be scored
        manually by the TAs.

        (2) The inputs may be either color or grayscale, but they will never be
        mixed; both images will either be color, or both will be grayscale.

        (3) You can modify this function however you see fit -- e.g., change
        input parameters, return values, etc. -- to develop your blending
        process.
    """
    kp1, kp2, matches = findMatchesBetweenImages(
        image_1, image_2, num_matches)
    homography = findHomography(kp1, kp2, matches)
    min_xy, max_xy = getBoundingCorners(image_1, image_2, homography)
    output_image = warpCanvas(image_1, homography, min_xy, max_xy)

    # Blend images together
    final_output_image = np.copy(output_image)

    min_xy = min_xy.astype(np.int)
    # Set Image 2 to the right location
    final_output_image[-min_xy[1]:-min_xy[1] + image_2.shape[0],
                 -min_xy[0]:-min_xy[0] + image_2.shape[1]] = image_2

    image_2_rows, image_2_cols = image_2.shape[:2]
    for row in range(image_2_rows):
        for col in range(image_2_cols):
            is_black_pixel = np.equal(output_image[-min_xy[1] + row, -min_xy[0] + col],
                                np.array([0,0,0])).all()
            if not is_black_pixel:
                # Take an average blend of the two images
                final_output_image[-min_xy[1] + row, -min_xy[0] + col] = \
                        (   # Need to support color ranges too
                            np.uint16(image_2[row, col]) +
                            np.uint16(output_image[-min_xy[1] + row, -min_xy[0] + col])
                        ) / 2

    return final_output_image
    # END OF FUNCTION
